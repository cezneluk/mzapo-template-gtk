# MZAPO Template GTK

Rozšířený template o alternativní zobrazování periférií na lokálním zařízení pomocí GTK. Cílem není nahracení plného fungování přípravku MZAPO, ale jako užitečná pomůcka v případech, kdy je potřeba vyřešit detaily v zobrazení, a tudíž není potřeba vytěžovat omezený počet dostupných desek.

## Podporované periférie

- [x] Zápis na grafický displej pomocí postupného zapsaní všech pixelů (příkaz 0x2C)
- [x] LED diody
- [x] LED pásek
- [x] Tři otočné voliče zobrazené pomocí posuvníků

Pro podporu dalších periférií (a více ovládacích příkazů displeje) se mě neváhejte kontaktovat, vytvořit isseu nebo přímo i vytvořit merge request.

## Sestavení

Projekt využívá Cmake z důvodu možnosti mít více separatních konfigurací na sestavení projektu dle cílové platformy.

### GTK (lokální)
Je potřeba mít nainstalovaný GTK 3, na Ubuntu/Debian lze nainstalovat pomocí `sudo apt install libgtk-3-dev`

```
mkdir -p build_gtk && cd build_gtk
cmake ..
make
```

### MZAPO
Pro sestavení pro MZAPO je potřeba mít křížový překladač gcc pro platformu arm-linux-gnueabihf, na Ubuntu/Debian lze nainstalovat pomocí `sudo apt install gcc-arm-linux-gnueabihf`

```
mkdir -p build_mzapo && cd build_mkapo
cmake -DCMAKE_TOOLCHAIN_FILE=../toolchains/mzapo.cmake ..
make
```

Pro fungování `make run` (ekvivalentní chování jako u původního projektu) je potřeba definovat proměnnou `TARGET_IP` (například `-DTARGET_IP=192.168.207`). Proxy jump přes postel.felk.cvut.cz ovlivňuje proměnná `SSH_PROXY_JUMP_USER`.

## Používání

Pro komunikaci s displejem se používájí stejné funkce z `mzapo_parlcd.c` jako v původním projektu. Pro interakci s LED a otočnými voliči se nacházejí příslušné funkce v `mzapo_perip.c`.
