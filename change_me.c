#define _POSIX_C_SOURCE 200112L

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_perip.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "mzapo_utils.h"

int main(int argc, char* argv[])
{
	printf("Hello world!\n");

	void* lcd_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	void* led_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	parlcd_hx8357_init(lcd_base);

	led1_set(led_base, 0xFF0000);
	led2_set(led_base, 0xFF0000);
	ledline_set(led_base, -1);

	parlcd_write_cmd(lcd_base, 0x2C);
	for (int y = 0; y < PARLCD_HEIGHT; y++)
	{
		for (int x = 0; x < PARLCD_WIDTH; x++)
		{
			parlcd_write_data(lcd_base, 0xF800);
		}
	}

	sleep_ms(10000);
	return 0;
}
