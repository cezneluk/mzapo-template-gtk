#include "mzapo_gtk.h"
#include "mzapo_parlcd.h"
#include "mzapo_utils.h"

MzapoGtkData mzapo_gtk_data = {0};

void init_mzapo_gtk(int* argc, char*** argv)
{
	gtk_init(argc, argv);

	GtkBuilder* builder = gtk_builder_new_from_resource("/mzapo_template_gtk/window.glade");
	gtk_builder_connect_signals(builder, NULL);

	mzapo_gtk_data.window = GTK_WINDOW(gtk_builder_get_object(builder, "window"));
	mzapo_gtk_data.lcd = GTK_DRAWING_AREA(gtk_builder_get_object(builder, "lcd"));
	mzapo_gtk_data.led1 = GTK_DRAWING_AREA(gtk_builder_get_object(builder, "led1"));
	mzapo_gtk_data.led2 = GTK_DRAWING_AREA(gtk_builder_get_object(builder, "led2"));

	mzapo_gtk_data.knobs[0] = GTK_SCALE(gtk_builder_get_object(builder, "knob_r"));
	mzapo_gtk_data.knobs[1] = GTK_SCALE(gtk_builder_get_object(builder, "knob_g"));
	mzapo_gtk_data.knobs[2] = GTK_SCALE(gtk_builder_get_object(builder, "knob_b"));

	mzapo_gtk_data.knobs_adjustment[0] = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "knob_r_adjustment"));
	mzapo_gtk_data.knobs_adjustment[1] = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "knob_g_adjustment"));
	mzapo_gtk_data.knobs_adjustment[2] = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "knob_b_adjustment"));

	mzapo_gtk_data.ledline = GTK_DRAWING_AREA(gtk_builder_get_object(builder, "ledline"));

	mzapo_gtk_data.knobs_data =
		((((uint8_t)gtk_adjustment_get_value(mzapo_gtk_data.knobs_adjustment[0])) & 0xFF) << 16) |
		((((uint8_t)gtk_adjustment_get_value(mzapo_gtk_data.knobs_adjustment[1])) & 0xFF) << 8) |
		(((uint8_t)gtk_adjustment_get_value(mzapo_gtk_data.knobs_adjustment[2])) & 0xFF);

	mzapo_gtk_data.lcd_data = calloc(PARLCD_WIDTH * PARLCD_HEIGHT, sizeof(uint16_t));
	mzapo_gtk_data.lcd_data_pointer = mzapo_gtk_data.lcd_data;
	mzapo_gtk_data.lcd_data_end = mzapo_gtk_data.lcd_data + PARLCD_WIDTH * PARLCD_HEIGHT;

	mzapo_gtk_data.lcd_surface =
		cairo_image_surface_create_for_data((unsigned char*)mzapo_gtk_data.lcd_data, CAIRO_FORMAT_RGB16_565,
											PARLCD_WIDTH, PARLCD_HEIGHT, PARLCD_WIDTH * sizeof(uint16_t));

	g_object_unref(builder);

	gtk_widget_show(GTK_WIDGET(mzapo_gtk_data.window));

	mzapo_gtk_data.initialized = 1;
	gtk_main();
}

void* init_thread_wrapper(void* data) { init_mzapo_gtk(NULL, NULL); }

GThread* init_mzapo_gtk_thread()
{
	GThread* thread = g_thread_new("gtk_ui", init_thread_wrapper, NULL);
	while (!mzapo_gtk_data.initialized)
	{
		sleep_ms(50);
	}
}

double led_cairo_r(uint32_t color) { return ((color >> 16) & 0xFF) / 255.0; }

double led_cairo_g(uint32_t color) { return ((color >> 8) & 0xFF) / 255.0; }

double led_cairo_b(uint32_t color) { return (color & 0xFF) / 255.0; }

void on_window_destroy()
{
	gtk_main_quit();
	exit(0);
}

void lcd_draw(GtkWidget* widget, cairo_t* cr, gpointer user_data)
{
	cairo_set_source_surface(cr, mzapo_gtk_data.lcd_surface, 0, 0);
	cairo_paint(cr);
}

void led1_draw(GtkWidget* widget, cairo_t* cr, gpointer user_data)
{
	cairo_set_source_rgb(cr, led_cairo_r(mzapo_gtk_data.led1_color), led_cairo_g(mzapo_gtk_data.led1_color),
						 led_cairo_b(mzapo_gtk_data.led1_color));
	cairo_paint(cr);
}

void led2_draw(GtkWidget* widget, cairo_t* cr, gpointer user_data)
{
	cairo_set_source_rgb(cr, led_cairo_r(mzapo_gtk_data.led2_color), led_cairo_g(mzapo_gtk_data.led2_color),
						 led_cairo_b(mzapo_gtk_data.led2_color));
	cairo_paint(cr);
}

void ledline_draw(GtkWidget* widget, cairo_t* cr, gpointer user_data)
{
	guint width = gtk_widget_get_allocated_width(widget);
	guint height = gtk_widget_get_allocated_width(widget);

	double led_width = width / 32.0;

	for (int i = 0; i < 32; i++)
	{
		int is_on = (mzapo_gtk_data.ledline_data >> i) & 0x01;
		if (is_on)
			cairo_set_source_rgb(cr, 1, 0, 0);
		else
			cairo_set_source_rgb(cr, 0, 0, 0);

		cairo_rectangle(cr, width - led_width * (i + 1), 0, led_width, height);
		cairo_fill(cr);
	}
}

void knob_r_changed(GtkAdjustment* adjustment, gpointer user_data)
{
	mzapo_gtk_data.knobs_data =
		((((uint8_t)gtk_adjustment_get_value(adjustment)) & 0xFF) << 16) | (mzapo_gtk_data.knobs_data & 0x00FFFFF);
}

void knob_g_changed(GtkAdjustment* adjustment, gpointer user_data)
{
	mzapo_gtk_data.knobs_data =
		((((uint8_t)gtk_adjustment_get_value(adjustment)) & 0xFF) << 8) | (mzapo_gtk_data.knobs_data & 0xFF00FF);
}

void knob_b_changed(GtkAdjustment* adjustment, gpointer user_data)
{
	mzapo_gtk_data.knobs_data =
		(((uint8_t)gtk_adjustment_get_value(adjustment)) & 0xFF) | (mzapo_gtk_data.knobs_data & 0xFFFF00);
}

int gtk_redraw_thread(void* widget)
{
	gtk_widget_queue_draw(*((GtkWidget**)widget));
	return 1;
}

void gtk_led1_set(uint32_t color)
{
	mzapo_gtk_data.led1_color = color;
	gdk_threads_add_idle(gtk_redraw_thread, &mzapo_gtk_data.led1);
}

void gtk_led2_set(uint32_t color)
{
	mzapo_gtk_data.led2_color = color;
	gdk_threads_add_idle(gtk_redraw_thread, &mzapo_gtk_data.led2);
}

void gtk_ledline_set(uint32_t data)
{
	mzapo_gtk_data.ledline_data = data;
	gdk_threads_add_idle(gtk_redraw_thread, &mzapo_gtk_data.ledline);
}

void gtk_lcd_reset_pointer() { mzapo_gtk_data.lcd_data_pointer = mzapo_gtk_data.lcd_data; }

void gtk_lcd_set_pointer(int offset) { mzapo_gtk_data.lcd_data_pointer = mzapo_gtk_data.lcd_data + offset; }

void gtk_lcd_set_pixel(uint16_t color)
{
	*(mzapo_gtk_data.lcd_data_pointer++) = color;
	if (mzapo_gtk_data.lcd_data_pointer >= mzapo_gtk_data.lcd_data_end) // redraw after whole buffer are written
		gdk_threads_add_idle(gtk_redraw_thread, &mzapo_gtk_data.lcd);
}

uint32_t gtk_knobs_get() { return mzapo_gtk_data.knobs_data; }