#ifndef MZAPO_GTK_H
#define MZAPO_GTK_H

#include <cairo/cairo.h>
#include <gtk/gtk.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

	typedef struct mzapo_gtk_data
	{
		int initialized;
		GtkWindow* window;

		GtkDrawingArea* lcd;
		GtkDrawingArea* led1;
		GtkDrawingArea* led2;

		GtkScale* knobs[3];
		GtkAdjustment* knobs_adjustment[3];

		GtkDrawingArea* ledline;

		uint32_t knobs_data;

		uint32_t led1_color;
		uint32_t led2_color;
		uint32_t ledline_data;

		uint16_t* lcd_data;
		uint16_t* lcd_data_pointer;
		uint16_t* lcd_data_end;

		cairo_surface_t* lcd_surface;
	} MzapoGtkData;

	void init_mzapo_gtk(int* argc, char*** argv);

	GThread* init_mzapo_gtk_thread();

	void gtk_led1_set(uint32_t color);

	void gtk_led2_set(uint32_t color);

	void gtk_ledline_set(uint32_t data);

	void gtk_lcd_reset_pointer();

	void gtk_lcd_set_pointer(int offset);

	void gtk_lcd_set_pixel(uint16_t color);

	uint32_t gtk_knobs_get();

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif