#define _POSIX_C_SOURCE 200112L

//#define HX8357_B
//#define ILI9481

#include <stdint.h>
#include <time.h>

#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_utils.h"

#ifdef MZAPO_GTK
#include "mzapo_gtk.h"
#include <stdio.h>
#endif

void parlcd_write_cr(unsigned char* parlcd_mem_base, uint16_t data)
{
#ifdef MZAPO_GTK
	fprintf(stderr, "parlcd_write_cr is not implemented in GTK mode\n");
#else
	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CR_o) = data;
#endif
}

void parlcd_write_cmd(unsigned char* parlcd_mem_base, uint16_t cmd)
{
#ifdef MZAPO_GTK
	if (cmd == 0x2C)
		gtk_lcd_reset_pointer();
	else
		fprintf(stderr, "Invalid lcd command (%d) in GTK mode\n", cmd);
#else
	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = cmd;
#endif
}

void parlcd_write_data(unsigned char* parlcd_mem_base, uint16_t data)
{
#ifdef MZAPO_GTK
	gtk_lcd_set_pixel(data);
#else
	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
#endif
}

void parlcd_write_data2x(unsigned char* parlcd_mem_base, uint32_t data)
{
#ifdef MZAPO_GTK
	gtk_lcd_set_pixel(data >> 16);
	gtk_lcd_set_pixel(data & 0xFFFF);
#else
	*(volatile uint32_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
#endif
}

void parlcd_hx8357_init(unsigned char* parlcd_mem_base)
{
#ifdef MZAPO_GTK
	init_mzapo_gtk_thread();
#else
	// toggle RST low to reset
	/*
		digitalWrite(_rst, HIGH);
		sleep_ms(50);
		digitalWrite(_rst, LOW);
		sleep_ms(10);
		digitalWrite(_rst, HIGH);
		sleep_ms(10);
	*/
	parlcd_write_cmd(parlcd_mem_base, 0x1);
	sleep_ms(30);

#if defined(ILI9481)
	// Configure ILI9481 display

	parlcd_write_cmd(parlcd_mem_base, 0x11);
	sleep_ms(20);
	parlcd_write_cmd(parlcd_mem_base, 0xD0);
	parlcd_write_data(parlcd_mem_base, 0x07);
	parlcd_write_data(parlcd_mem_base, 0x42);
	parlcd_write_data(parlcd_mem_base, 0x18);

	parlcd_write_cmd(parlcd_mem_base, 0xD1);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x07);
	parlcd_write_data(parlcd_mem_base, 0x10);

	parlcd_write_cmd(parlcd_mem_base, 0xD2);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0x02);

	parlcd_write_cmd(parlcd_mem_base, 0xC0);
	parlcd_write_data(parlcd_mem_base, 0x10);
	parlcd_write_data(parlcd_mem_base, 0x3B);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x02);
	parlcd_write_data(parlcd_mem_base, 0x11);

	parlcd_write_cmd(parlcd_mem_base, 0xC5);
	parlcd_write_data(parlcd_mem_base, 0x03);

	parlcd_write_cmd(parlcd_mem_base, 0xC8);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x32);
	parlcd_write_data(parlcd_mem_base, 0x36);
	parlcd_write_data(parlcd_mem_base, 0x45);
	parlcd_write_data(parlcd_mem_base, 0x06);
	parlcd_write_data(parlcd_mem_base, 0x16);
	parlcd_write_data(parlcd_mem_base, 0x37);
	parlcd_write_data(parlcd_mem_base, 0x75);
	parlcd_write_data(parlcd_mem_base, 0x77);
	parlcd_write_data(parlcd_mem_base, 0x54);
	parlcd_write_data(parlcd_mem_base, 0x0C);
	parlcd_write_data(parlcd_mem_base, 0x00);

	parlcd_write_cmd(parlcd_mem_base, 0x36);
	// parlcd_write_data(parlcd_mem_base, 0x0A);
	parlcd_write_data(parlcd_mem_base, 0x28);

	parlcd_write_cmd(parlcd_mem_base, 0x3A);
	parlcd_write_data(parlcd_mem_base, 0x55);

	// parlcd_write_cmd(parlcd_mem_base, 0x2A);
	parlcd_write_cmd(parlcd_mem_base, 0x2B);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0x3F);

	// parlcd_write_cmd(parlcd_mem_base, 0x2B);
	parlcd_write_cmd(parlcd_mem_base, 0x2A);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0xDF);

	sleep_ms(120);
	parlcd_write_cmd(parlcd_mem_base, 0x29);

	sleep_ms(25);

#elif defined(HX8357_B)
	// Configure HX8357-B display
	parlcd_write_cmd(parlcd_mem_base, 0x11);
	sleep_ms(20);
	parlcd_write_cmd(parlcd_mem_base, 0xD0);
	parlcd_write_data(parlcd_mem_base, 0x07);
	parlcd_write_data(parlcd_mem_base, 0x42);
	parlcd_write_data(parlcd_mem_base, 0x18);

	parlcd_write_cmd(parlcd_mem_base, 0xD1);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x07);
	parlcd_write_data(parlcd_mem_base, 0x10);

	parlcd_write_cmd(parlcd_mem_base, 0xD2);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0x02);

	parlcd_write_cmd(parlcd_mem_base, 0xC0);
	parlcd_write_data(parlcd_mem_base, 0x10);
	parlcd_write_data(parlcd_mem_base, 0x3B);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x02);
	parlcd_write_data(parlcd_mem_base, 0x11);

	parlcd_write_cmd(parlcd_mem_base, 0xC5);
	parlcd_write_data(parlcd_mem_base, 0x08);

	parlcd_write_cmd(parlcd_mem_base, 0xC8);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x32);
	parlcd_write_data(parlcd_mem_base, 0x36);
	parlcd_write_data(parlcd_mem_base, 0x45);
	parlcd_write_data(parlcd_mem_base, 0x06);
	parlcd_write_data(parlcd_mem_base, 0x16);
	parlcd_write_data(parlcd_mem_base, 0x37);
	parlcd_write_data(parlcd_mem_base, 0x75);
	parlcd_write_data(parlcd_mem_base, 0x77);
	parlcd_write_data(parlcd_mem_base, 0x54);
	parlcd_write_data(parlcd_mem_base, 0x0C);
	parlcd_write_data(parlcd_mem_base, 0x00);

	parlcd_write_cmd(parlcd_mem_base, 0x36);
	parlcd_write_data(parlcd_mem_base, 0x0a);

	parlcd_write_cmd(parlcd_mem_base, 0x3A);
	parlcd_write_data(parlcd_mem_base, 0x55);

	parlcd_write_cmd(parlcd_mem_base, 0x2A);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0x3F);

	parlcd_write_cmd(parlcd_mem_base, 0x2B);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0xDF);

	sleep_ms(120);
	parlcd_write_cmd(parlcd_mem_base, 0x29);

	sleep_ms(25);

#else
	// HX8357-C display initialisation

	parlcd_write_cmd(parlcd_mem_base, 0xB9); // Enable extension command
	parlcd_write_data(parlcd_mem_base, 0xFF);
	parlcd_write_data(parlcd_mem_base, 0x83);
	parlcd_write_data(parlcd_mem_base, 0x57);
	sleep_ms(50);

	parlcd_write_cmd(parlcd_mem_base, 0xB6); // Set VCOM voltage
	// parlcd_write_data(parlcd_mem_base, 0x2C);    //0x52 for HSD 3.0"
	parlcd_write_data(parlcd_mem_base, 0x52); // 0x52 for HSD 3.0"

	parlcd_write_cmd(parlcd_mem_base, 0x11); // Sleep off
	sleep_ms(200);

	parlcd_write_cmd(parlcd_mem_base, 0x35);  // Tearing effect on
	parlcd_write_data(parlcd_mem_base, 0x00); // Added parameter

	parlcd_write_cmd(parlcd_mem_base, 0x3A);  // Interface pixel format
	parlcd_write_data(parlcd_mem_base, 0x55); // 16 bits per pixel

	// parlcd_write_cmd(parlcd_mem_base, 0xCC); // Set panel characteristic
	// parlcd_write_data(parlcd_mem_base, 0x09);    // S960>S1, G1>G480, R-G-B, normally black

	// parlcd_write_cmd(parlcd_mem_base, 0xB3); // RGB interface
	// parlcd_write_data(parlcd_mem_base, 0x43);
	// parlcd_write_data(parlcd_mem_base, 0x00);
	// parlcd_write_data(parlcd_mem_base, 0x06);
	// parlcd_write_data(parlcd_mem_base, 0x06);

	parlcd_write_cmd(parlcd_mem_base, 0xB1); // Power control
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x15);
	parlcd_write_data(parlcd_mem_base, 0x0D);
	parlcd_write_data(parlcd_mem_base, 0x0D);
	parlcd_write_data(parlcd_mem_base, 0x83);
	parlcd_write_data(parlcd_mem_base, 0x48);

	parlcd_write_cmd(parlcd_mem_base, 0xC0); // Does this do anything?
	parlcd_write_data(parlcd_mem_base, 0x24);
	parlcd_write_data(parlcd_mem_base, 0x24);
	parlcd_write_data(parlcd_mem_base, 0x01);
	parlcd_write_data(parlcd_mem_base, 0x3C);
	parlcd_write_data(parlcd_mem_base, 0xC8);
	parlcd_write_data(parlcd_mem_base, 0x08);

	parlcd_write_cmd(parlcd_mem_base, 0xB4); // Display cycle
	parlcd_write_data(parlcd_mem_base, 0x02);
	parlcd_write_data(parlcd_mem_base, 0x40);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x2A);
	parlcd_write_data(parlcd_mem_base, 0x2A);
	parlcd_write_data(parlcd_mem_base, 0x0D);
	parlcd_write_data(parlcd_mem_base, 0x4F);

	parlcd_write_cmd(parlcd_mem_base, 0xE0); // Gamma curve
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x15);
	parlcd_write_data(parlcd_mem_base, 0x1D);
	parlcd_write_data(parlcd_mem_base, 0x2A);
	parlcd_write_data(parlcd_mem_base, 0x31);
	parlcd_write_data(parlcd_mem_base, 0x42);
	parlcd_write_data(parlcd_mem_base, 0x4C);
	parlcd_write_data(parlcd_mem_base, 0x53);
	parlcd_write_data(parlcd_mem_base, 0x45);
	parlcd_write_data(parlcd_mem_base, 0x40);
	parlcd_write_data(parlcd_mem_base, 0x3B);
	parlcd_write_data(parlcd_mem_base, 0x32);
	parlcd_write_data(parlcd_mem_base, 0x2E);
	parlcd_write_data(parlcd_mem_base, 0x28);

	parlcd_write_data(parlcd_mem_base, 0x24);
	parlcd_write_data(parlcd_mem_base, 0x03);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x15);
	parlcd_write_data(parlcd_mem_base, 0x1D);
	parlcd_write_data(parlcd_mem_base, 0x2A);
	parlcd_write_data(parlcd_mem_base, 0x31);
	parlcd_write_data(parlcd_mem_base, 0x42);
	parlcd_write_data(parlcd_mem_base, 0x4C);
	parlcd_write_data(parlcd_mem_base, 0x53);
	parlcd_write_data(parlcd_mem_base, 0x45);
	parlcd_write_data(parlcd_mem_base, 0x40);
	parlcd_write_data(parlcd_mem_base, 0x3B);
	parlcd_write_data(parlcd_mem_base, 0x32);

	parlcd_write_data(parlcd_mem_base, 0x2E);
	parlcd_write_data(parlcd_mem_base, 0x28);
	parlcd_write_data(parlcd_mem_base, 0x24);
	parlcd_write_data(parlcd_mem_base, 0x03);
	parlcd_write_data(parlcd_mem_base, 0x00);
	parlcd_write_data(parlcd_mem_base, 0x01);

	parlcd_write_cmd(parlcd_mem_base, 0x36); // MADCTL Memory access control
	// parlcd_write_data(parlcd_mem_base, 0x48);
	parlcd_write_data(parlcd_mem_base, 0xE8);
	sleep_ms(20);

	parlcd_write_cmd(parlcd_mem_base, 0x21); // Display inversion on
	sleep_ms(20);

	parlcd_write_cmd(parlcd_mem_base, 0x29); // Display on

	sleep_ms(120);
#endif
#endif
}
