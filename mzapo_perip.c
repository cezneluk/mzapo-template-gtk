#include "mzapo_regs.h"

#include <stdint.h>

#ifdef MZAPO_GTK
#include "mzapo_gtk.h"
#include <stdio.h>
#endif

void ledline_set(unsigned char* led_mem_base, uint32_t data)
{
#ifdef MZAPO_GTK
	gtk_ledline_set(data);
#else
	*(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_LINE_o) = data;
#endif
}

void led1_set(unsigned char* led_mem_base, uint32_t color)
{
#ifdef MZAPO_GTK
	gtk_led1_set(color);
#else
	*(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_RGB1_o) = color;
#endif
}

void led2_set(unsigned char* led_mem_base, uint32_t color)
{
#ifdef MZAPO_GTK
	gtk_led2_set(color);
#else
	*(volatile uint32_t*)(led_mem_base + SPILED_REG_LED_RGB2_o) = color;
#endif
}

uint32_t knobs_get(unsigned char* led_mem_base)
{
#ifdef MZAPO_GTK
	return gtk_knobs_get();
#else
	return *(volatile uint32_t*)(led_mem_base + SPILED_REG_KNOBS_8BIT_o);
#endif
}