#ifndef MZAPO_PERIP_H
#define MZAPO_PERIP_H

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

	void ledline_set(unsigned char* led_mem_base, uint32_t data);

	void led1_set(unsigned char* led_mem_base, uint32_t color);

	void led2_set(unsigned char* led_mem_base, uint32_t color);

	uint32_t knobs_get(unsigned char* led_mem_base);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif