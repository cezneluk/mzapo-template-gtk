#define _POSIX_C_SOURCE 200112L
#include <time.h>

void sleep_ms(int msec)
{
	struct timespec wait_delay = {.tv_sec = msec / 1000, .tv_nsec = (msec % 1000) * 1000 * 1000};
	clock_nanosleep(CLOCK_MONOTONIC, 0, &wait_delay, NULL);
}